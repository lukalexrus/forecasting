package com.forecasting;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


@Path("/parameters")
public class MainService {

   private static final String API_VERSION = "0.0.2";
   
   @GET
   @Path("")
   @Produces(MediaType.APPLICATION_JSON)
   public JSONObject postMessge(@QueryParam("val1") double val1, @QueryParam("val2") double val2,@QueryParam("val3") double val3, @QueryParam("Tm") double Tm) {
	   RandomProcess Rp = new RandomProcess();

	   double S[][] = Rp.calcY(val1, val2, val3, Tm);
	   double X[] = Rp.getX();
	   double Y[][] = new double [4][1000];
	   String jsonString[][] = new String [4][1000];
	   for (int m = 0; m < 4; m++){
		   Y[m] = Rp.getY(m);
			for (int l = 0; l < 1000; l++){
				jsonString[m][l] = String.valueOf(S[m][l]);
			}
	   }
	   
	   JSONObject returnObject = new JSONObject();
	   
	   returnObject.put("X", X);
	   returnObject.put("S", S);
	   
	   Winner winner = new Winner();
	   winner.setT(Tm);
	   
	   double[] W0 = winner.fakeResult(S[0]);
	   double[] W1 = winner.fakeResult(S[1]);
	   double[] W2 = winner.fakeResult(S[2]);
	   double[] W3 = winner.fakeResult(S[3]);
	   
	   returnObject.put("Y0", W0);
	   returnObject.put("Y1", W1);
	   returnObject.put("Y2", W2);
	   returnObject.put("Y3", W3);
	   
	   double[] dW0 = winner.errorForecasting(W0, S[0]);
	   double[] dW1 = winner.errorForecasting(W1, S[1]);
	   double[] dW2 = winner.errorForecasting(W2, S[2]);
	   double[] dW3 = winner.errorForecasting(W3, S[3]);
	   
	   returnObject.put("dW0", dW0);
	   returnObject.put("dW1", dW1);
	   returnObject.put("dW2", dW2);
	   returnObject.put("dW3", dW3);
	   
	   LMS lms = new LMS();
	   lms.setT(Tm);
	   
	   double[] LMS0 = lms.fakeResult(S[0]);
	   double[] LMS1 = lms.fakeResult(S[1]);
	   double[] LMS2 = lms.fakeResult(S[2]);
	   double[] LMS3 = lms.fakeResult(S[3]);
	   
	   returnObject.put("LMS0", LMS0);
	   returnObject.put("LMS1", LMS1);
	   returnObject.put("LMS2", LMS2);
	   returnObject.put("LMS3", LMS3);
	   
	   double[] dLMS0 = lms.errorForecasting(LMS0, S[0]);
	   double[] dLMS1 = lms.errorForecasting(LMS1, S[1]);
	   double[] dLMS2 = lms.errorForecasting(LMS2, S[2]);
	   double[] dLMS3 = lms.errorForecasting(LMS3, S[3]);
	   
	   returnObject.put("dLMS0", dLMS0);
	   returnObject.put("dLMS1", dLMS1);
	   returnObject.put("dLMS2", dLMS2);
	   returnObject.put("dLMS3", dLMS3);
	   
	   return returnObject;
   }
}