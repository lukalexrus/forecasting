package com.forecasting;

public class RandomProcess {
	private static final int Fs = 1;
	private static final int Fd = 100;
	private static final int j = 4;
	private static final int n = 1000;
	
	private double A[] = new double[j];
	private double B[] = new double[j];
	private double X[] = new double[n];
	private double Y[][] = new double[j][n];

	public double[] getY(int i) {
		return Y[i];
	}

	public double[] getX() {
		return X;
	}

	private double calcTs() {
		return 1.0 / (2 * Math.PI * Fs);
	}
	
	private double calcTd() {
		return 1.0 / Fd;
	}
	
	public double[][] calcY(double val1, double val2, double val3, double Tm) {
		if (Tm == 0){
			Tm = 1;
		}
		int k = 0;

		double S[][] = new double[j][n];
		double Km[][] = new double[j][n];

		double Ts = calcTs();
		double Td = calcTd();
		
		for (int i = 0 ; i < j; i++){
			A[i] = Td / (Td + Ts);
			B[i] = Ts / (Td + Ts);		
		}
		
		for (double i = -1; i <= 1 ; i += 0.002){
			X[k] = i;
			k++;
		}
		
		for (int l = 0; l < n; l++) {
			Km[0][l] = 1;
			Km[1][l] = 1 + val1 * l / Tm;
			Km[2][l] = 1 + val2 * Math.pow((l / Tm), 2);
			Km[3][l] = val3 * Math.exp(l / Tm);
		}
		
		for (int m = 0; m < j; m++){
			for (int l = 0; l < n; l++){
				if (l == 0) {
					Y[m][l] = 0;
					S[m][l] = 0;
				}
				else {
					Y[m][l] = A[m]* X[l] + B[m]*Y[m][l - 1];
					
					S[m][l] = Km[m][l] * (A[m] * Y[m][l] + B[m] * S[m][l - 1]);
				}
			}
		}
		return S;
	}
}
