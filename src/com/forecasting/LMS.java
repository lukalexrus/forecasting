package com.forecasting;

public class LMS {
	private static final int n = 1000;
	private static final double u = 0.04;
	private static final int M = 1000;
	private int T;
	

	public void setT(double t) {
		T = (int) t;
	}

	private double randomX() {
		return (Math.random() * 3) - 1.5;
	}
	
	public double[] fakeResult(double X[]) {
		double Y[] = new double [n];
		for (int i= 0; i < n; i++ ) {
			if (i == 0) {
				Y[i] = 0;
			}
			else {
				Y[i] = randomX() + X[i];
			}
		}
		return Y;
	}
	
	public double[] calcLMS(double X[], double Y[], int j) {
		double e[] = new double [n];
		double W[][] = new double [n][n];
		double calculationY[] = new double [n];
		
		for(int k = 10; k < M; k++) {
			if (k - T >= 0) {
				e[k] = X[k] - Y [k - T];
			}
			else {
				e[k] = X[k];
			}
			
		}
		
		for (int k = 10; k < M; k++) {
			for (int i = 0; i < 10; i++) {
				if (k - i >= 0) {
					W[i][k] = W[i][k-1] + u * e[k] * X[k - i -1];
				}
			}
		}
		
		for (int k = 0; k < n; k++){
			for (int i = 0; i < n; i++){
				if (k - i > 0){
					calculationY [k] += (W[i][k] * X[k - i]);
				}
			}
		}
		return calculationY;
	}
	
	public double[] errorForecasting(double S[], double Y[]) {
		 double d[] = new double [n];
		  for (int k = 0; k < n; k++)
         {
             d[k] = S[k] - Y[k];
         }
         return d;
	}
}
