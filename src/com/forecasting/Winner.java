package com.forecasting;

import Jama.Matrix;

public class Winner {
	private static final int n = 1000;
	private static final int N = 10;
	private static final int M = 1000;
	private int T;
	private static final int L = 1000;
	
	private double R[] = new double[n];
	private double Rx[][] = new double[n][n];
	private double Pz[] = new double[n];
	private double W[] = new double[n];
	
	public void setT(double val) {
		T = (int) val;
	}

	private double randomX() {
		return Math.random() - 0.5;
	}
	
	public double[] fakeResult(double X[]) {
		double Y[] = new double [n];
		for (int i= 0; i < n; i++ ) {
			if (i == 0) {
				Y[i] = 0;
			}
			else {
				Y[i] = randomX() + X[i];
			}
		}
		return Y;
	}
	
	public double [] calcWinner(double X[], int jValue) {
		fullRx(X, jValue);
		vzaimKor(X);
		parametrsForecastingFilter();
		return forecastingValue(X);
	}
	
	private void korR(double X[], int jValue)
     {
         double temp = 0;

         for (int j = 0; j < n; j++)
         {
             for (int k = N; k <= M - T; k++)
             {
            	 if (k - jValue >= 0) {
                     temp += X[k] * X[k - jValue];
            	 }
            	 else {
            		 temp += X[k];
            	 }
             }
             R[j] = (temp / (M - N - T));
         }
     }

	private void fullRx(double X[], int jValue)
     {
         korR(X, jValue);
         for (int i = 0; i < n; i++)
         {
             for (int j = 0; j < n; j++)
             {
                 Rx[i][j] = R[(i + j) % n];
             }
         }
     }
     
	private void vzaimKor(double X[])
     {
         double temp = 0;

         for (int j = 0; j < n; j++)
         {
             for (int k = N; k < M - T; k++)
             {
            	 if (k + T <= n && k - j >= 0) {
//            		 System.out.println( X[k + T]);
//            		 System.out.println("Xj " + X[k - j]);
                     temp += X[k + T] * X[k - j]; 
            	 }
             }
             Pz[j] = (temp / (M - N - T));
             
         }
     }
     
	private void parametrsForecastingFilter() {
//		System.out.println(Rx[0][2]);
		Matrix a = new Matrix(Rx);
		double[][] inverseRX =  a.inverse().getArray();
		for (int j = 0; j < n; j++)
        {
            for (int k = 0; k < n; k++)
            {
                W[j] += inverseRX[j][k] * Pz[k];
            }
        }
	}
	
	private double [] forecastingValue(double X[])//�������� ������������
    {
		double Y[] = new double[n];
        for (int k = 0; k < n; k++)
        {
            for (int j = 0; j < N; j++)
            {
            	if (k - j >= 0){
            		Y[k] += W[j] * X[k - j];
            	}
            }
        }
        return Y;
    }
	
	public double[] errorForecasting(double S[], double Y[]) {
		 double d[] = new double [n];
		  for (int k = 0; k < n; k++)
          {
              d[k] = S[k] - Y[k];
          }
          return d;
	}
}
